﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Serialization
{
    public class Score
    {
        // Constructeur vide pour la sérialisation
        public Score()
        {
            HomeScore = 0;
            AwayScore = 0;
            Winner = 0;
        }
        // Constructeur de score existant
        public Score(int homeScore, int awayScore)
        {
            HomeScore = homeScore;
            AwayScore = awayScore;
            ScoreWinner();
        }
        [XmlElement(ElementName = "Home")]
        public int HomeScore { get; set; }
        [XmlElement(ElementName = "Away")]
        public int AwayScore { get; set; }
        [XmlIgnore]
        public int Winner { get; set; }
        private void ScoreWinner()
        {
            Winner = 0;
            if (HomeScore > AwayScore)
                Winner = 1;
            else if (HomeScore < AwayScore)
                Winner = -1;
        }
        public void OnDeserialized()
        {
            ScoreWinner();
        }
        public void Presentation()
        {
            Console.WriteLine("Score : {0}-{1} ; vainqueur : {2}", HomeScore, AwayScore, Winner);
        }
    }
    public class Game : Score
    {
        // Constructeur de jeu vide pour la sérialisation
        public Game() : base() { }
        public Game(int homeScore, int awayScore) : base(homeScore, awayScore) { }
    }
    public class Games : List<Game>
    {
        public Games() { }
    }
}
