﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Serialization
{
    public class InterclubMatch
    {
        public InterclubMatch() { }
        public InterclubMatch(DateTime date, int matchNumber, InterclubTeam homeTeam, InterclubTeam awayTeam)
        {
            Date = date;
            MatchNumber = matchNumber;
            HomeTeam = homeTeam;
            AwayTeam = awayTeam;
            Matches = GenerateMatches();
            InterclubScore = null;
        }
        public DateTime Date { get; set; }
        [XmlAttribute]
        public int MatchNumber { get; set; }
        public InterclubTeam HomeTeam { get; set; }
        public InterclubTeam AwayTeam { get; set; }
        public Matches Matches { get; set; }
        public Score InterclubScore { get; set; }
        public Matches GenerateMatches()
        {
            Matches matches = null;
            if ((HomeTeam.Players != null)&&(AwayTeam.Players != null))
            {
                matches = new Matches();
                matches.Add(new Match(HomeTeam.Players.ElementAt(3), AwayTeam.Players.ElementAt(1)));
                matches.Add(new Match(HomeTeam.Players.ElementAt(2), AwayTeam.Players.ElementAt(0)));
                matches.Add(new Match(HomeTeam.Players.ElementAt(1), AwayTeam.Players.ElementAt(3)));
                matches.Add(new Match(HomeTeam.Players.ElementAt(0), AwayTeam.Players.ElementAt(2)));
                matches.Add(new Match(HomeTeam.Players.ElementAt(3), AwayTeam.Players.ElementAt(0)));
                matches.Add(new Match(HomeTeam.Players.ElementAt(2), AwayTeam.Players.ElementAt(1)));
                matches.Add(new Match(HomeTeam.Players.ElementAt(1), AwayTeam.Players.ElementAt(2)));
                matches.Add(new Match(HomeTeam.Players.ElementAt(0), AwayTeam.Players.ElementAt(3)));
                matches.Add(new Match(HomeTeam.Players.ElementAt(3), AwayTeam.Players.ElementAt(2)));
                matches.Add(new Match(HomeTeam.Players.ElementAt(2), AwayTeam.Players.ElementAt(3)));
                matches.Add(new Match(HomeTeam.Players.ElementAt(1), AwayTeam.Players.ElementAt(0)));
                matches.Add(new Match(HomeTeam.Players.ElementAt(0), AwayTeam.Players.ElementAt(1)));
                matches.Add(new Match(HomeTeam.Players.ElementAt(3), AwayTeam.Players.ElementAt(3)));
                matches.Add(new Match(HomeTeam.Players.ElementAt(2), AwayTeam.Players.ElementAt(2)));
                matches.Add(new Match(HomeTeam.Players.ElementAt(1), AwayTeam.Players.ElementAt(1)));
                matches.Add(new Match(HomeTeam.Players.ElementAt(0), AwayTeam.Players.ElementAt(0)));
            }
            return matches;
        }
        public void OnDeserialized(StrengthList listPlayers, ListOfTeams listTeams)
        {
            HomeTeam.OnDeserialized(listPlayers, listTeams);
            AwayTeam.OnDeserialized(listPlayers, listTeams);
            foreach (Match match in Matches)
            {
                match.OnDeserialized(listPlayers);
            }
            InterclubScore?.OnDeserialized();
        }
        public void Presentation()
        {
            Console.WriteLine("[{0}] {1} - {2} ({3})", MatchNumber, HomeTeam.TeamName.CompleteName, AwayTeam.TeamName.CompleteName, Date);
            Console.WriteLine("Home team :");
            HomeTeam.Presentation();
            Console.WriteLine("Away team :");
            AwayTeam.Presentation();
            Matches.Presentation();
            InterclubScore?.Presentation();
        }
    }
}
