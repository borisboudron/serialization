﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace Serialization
{
    class Program
    {
        static void Main(string[] args)
        {
            //Stream stream = new Stream();
            //XmlSerializer serializer = new XmlSerializer(typeof(PingDataBase));
            XmlSerializer serializer = new XmlSerializer(typeof(InterclubMatchDay));
            TextWriter writer = new StreamWriter("D:\\Desktop\\test.xml");
			// Création de la PingDatabase test
			//////////////////////////////////////////////// Import Players from json
			StrengthList listPlayers = new StrengthList();
            //listPlayers.Add(charles);
            //listPlayers.Add(boris);
            //listPlayers.Add(quentin);
            //listPlayers.Add(raphael);

            ListOfClubTeams listTeams = new ListOfClubTeams();
            Serie p2A = new Serie('P', 2, 'A');
            Serie p3E = new Serie('P', 3, 'E');
            ClubTeam theuxA = new ClubTeam("Theux", 'A', 9, p2A);
            ClubTeam theuxB = new ClubTeam("Theux", 'B', 9, p3E);
            listTeams.Add(theuxA);


            PingDataBase pingDB = new PingDataBase(2018, "TT Theux", "Theux", listPlayers, listTeams);
            Game game1 = new Game(11, 9);
            Game game2 = new Game(11, 13);
            Games games1 = new Games { game1, game2, game1 };
            Games games2 = new Games { game2, game1, game2, game1, game1 };
            //Match match1 = new Match(boris, charles, games2);
            //Match match2 = new Match(boris, quentin, games1);
            //Matches matches = new Matches { match1, match2 };

            //InterclubTeam team1 = new InterclubTeam(theuxA, new List<Player> { quentin, boris, raphael, charles });
            //InterclubTeam team2 = new InterclubTeam(theuxB, new List<Player> { raphael, charles, quentin, boris });
            InterclubTeam team3 = new InterclubTeam(theuxA);
            InterclubTeam team4 = new InterclubTeam(theuxB);
            //InterclubMatch iMatch1 = new InterclubMatch(System.DateTime.Now, 015354, team1, team2);
            //iMatch1.Matches.ElementAt(0).Games = games2;
            //iMatch1.Matches.ElementAt(0).MatchScore = new Score(3, 2);
            InterclubMatch iMatch2 = new InterclubMatch(System.DateTime.Now, 015355, team3, team4);

            //InterclubMatchDay iMatchDay = new InterclubMatchDay(1, System.DateTime.Now, new List<InterclubMatch> { iMatch1, iMatch2 });
            //serializer.Serialize(writer, iMatchDay);
            writer.Close();
            //iMatch1.Presentation();

            XmlSerializer serializer2 = new XmlSerializer(typeof(InterclubMatchDay));
            FileStream fs = new FileStream("D:\\Desktop\\test.xml", FileMode.Open);
            XmlReader reader = XmlReader.Create(fs);
            //PingDataBase pingDB2 = new PingDataBase();
            InterclubMatchDay iMatchDayT = new InterclubMatchDay();
            //pingDB2 = (PingDataBase)serializer2.Deserialize(reader);
            iMatchDayT = (InterclubMatchDay)serializer2.Deserialize(reader);
            iMatchDayT.OnDeserialized(listPlayers, new ListOfTeams { theuxA, theuxB });
            fs.Close();
            iMatchDayT.Presentation();
        }
        static void RealData()
        {
            string longName = "TT Theux";
            string clubName = "Theux";
            // Liste de force
            StrengthList lfTheux = new StrengthList
            {new ClubPlayer("117954", "Formatin", "Jean-Marie", Ranking.B4),
             new ClubPlayer("", "(Boury)", "(Charles)", Ranking.B6),
             new ClubPlayer("119330", "Schyns", "Serge", Ranking.C0),
             new ClubPlayer("126715", "Bouhon", "Laurent", Ranking.C4),
             new ClubPlayer("123680", "Moulinier", "Ludovic", Ranking.C4),
             new ClubPlayer("118512", "Schyns", "Lucien", Ranking.C4),
             new ClubPlayer("", "(Carol)", "(Pierre)", Ranking.C6),
             new ClubPlayer("118509", "Saulle", "Dante", Ranking.C6),
             new ClubPlayer("", "(Duez)", "(François)", Ranking.D0),
             new ClubPlayer("", "(Dumoulin)", "(Raphaël)", Ranking.D0),
             new ClubPlayer("", "(Méant)", "(Brice)", Ranking.D0),
             new ClubPlayer("118614", "Monville", "Michel", Ranking.D0),
             new ClubPlayer("", "(Vanbrabant)", "(Quentin)", Ranking.D0),
             new ClubPlayer("118514", "Wera", "Jean", Ranking.D0),
             new ClubPlayer("", "(Delaive)", "(Loïc)", Ranking.D2),
             new ClubPlayer("128231", "Boudron", "Boris", Ranking.D4),
             new ClubPlayer("153754", "De La Cerda", "Pedro", Ranking.D4),
             new ClubPlayer("", "(Pironnet)", "(Daniel)", Ranking.D6),
             new ClubPlayer("131021", "Bouhon", "Christian", Ranking.E2),
             new ClubPlayer("147384", "Lecloux", "Auxane", Ranking.E2),
             new ClubPlayer("118619", "Pironnet", "Didier", Ranking.E2),
             new ClubPlayer("154939", "Delaive", "Quentin", Ranking.E4),
             new ClubPlayer("151010", "Fays", "Olivier", Ranking.E4),
             new ClubPlayer("156708", "Hazée", "Noah", Ranking.E6),
             new ClubPlayer("160348", "Dewit", "Vincent", Ranking.NC) };

            ClubTeam theuxA = new ClubTeam(clubName, 'A', 12);
            ClubTeam theuxB = new ClubTeam(clubName, 'B', 12);
            ClubTeam theuxC = new ClubTeam(clubName, 'C', 12);
            ClubTeam theuxD = new ClubTeam(clubName, 'D', 9);
            ClubTeam theuxE = new ClubTeam(clubName, 'E', 9);
            ClubTeam theuxF = new ClubTeam(clubName, 'F', 9);
            ClubTeam theuxG = new ClubTeam(clubName, 'G', 12);

            theuxA.TeamSerie = new Serie('P', 2, 'A');
            theuxB.TeamSerie = new Serie('P', 3, 'E');
            theuxC.TeamSerie = new Serie('P', 4, 'G');
            theuxD.TeamSerie = new Serie('P', 5, 'G');
            theuxE.TeamSerie = new Serie('P', 5, 'F');
            theuxF.TeamSerie = new Serie('P', 6, 'H');
            theuxG.TeamSerie = new Serie('P', 7, 'C');

            //PingDataBase realDB = new PingDataBase(2018, "TT Theux", lfTheux, );
        }
    }
}
