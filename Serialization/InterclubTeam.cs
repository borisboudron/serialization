﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Serialization
{
    // Cette classe n'héritera pas de Team ais comprendra un membreTeam ;
    // en effet, il faut qu'un Team puisse différencier 2 équipes de 2 journées différentes
    public class InterclubTeam
    {
        public InterclubTeam() { }
        public InterclubTeam(Team teamName)
        {
            TeamName = teamName;
            Name = teamName.CompleteName;
            Players = null;
            PlayersAN = null;
        }
        public InterclubTeam(Team teamName, List<Player> players)
        {
            TeamName = teamName;
            Name = teamName.CompleteName;
            Players = players;
            PlayersAN = ExtractANs();
        }
        [XmlIgnore]
        public Team TeamName { get; set; }
        [XmlAttribute(AttributeName = "TeamName")]
        public string Name { get; set; }
        [XmlIgnore]
        public List<Player> Players { get; set; }
        [XmlElement(ElementName = "Players")]
        public List<string> PlayersAN { get; set; }
        public void OnDeserialized(StrengthList listPlayers, ListOfTeams teams)
        {
            TeamName = teams.SearchInTeamList(Name);
            List<Player> players = new List<Player>();
            foreach (string player in PlayersAN)
            {
                players.Add(listPlayers.SearchByAN(player));
            }
            Players = players;
        }
        public void Presentation()
        {
            TeamName.Presentation();
            foreach (Player player in Players)
            {
                player.Presentation();
            }
        }
        public List<string> ExtractANs()
        {
            List<string> result = new List<string> ();
            foreach (Player player in Players)
            {
                result.Add(player.AffiliationNumber);
            }
            return result;
        }
    }
}
