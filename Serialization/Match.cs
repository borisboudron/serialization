﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Serialization
{
    public class Match
    {
        public Match()
        {
            HomePlayer = null;
            Home = "";
            AwayPlayer = null;
            Away = "";
            Games = null;
            MatchScore = null;
        }
        public Match(Player homePlayer, Player awayPlayer)
        {
            HomePlayer = homePlayer;
            Home = homePlayer.AffiliationNumber;
            AwayPlayer = awayPlayer;
            Away = awayPlayer.AffiliationNumber;
            Games = null;
            MatchScore = null;
        }
        public Match(Player homePlayer, Player awayPlayer, Games games)
        {
            HomePlayer = homePlayer;
            Home = homePlayer.AffiliationNumber;
            AwayPlayer = awayPlayer;
            Away = awayPlayer.AffiliationNumber;
            Games = games;
            MatchScore = CheckAndSetResult();
        }
        [XmlIgnore]
        public Player HomePlayer { get; set; }
        [XmlElement(ElementName = "HomePlayer")]
        public string Home { get; set; }
        [XmlIgnore]
        public Player AwayPlayer { get; set; }
        [XmlElement(ElementName = "AwayPlayer")]
        public string Away { get; set; }
        public Games Games { get; set; }
        public Score MatchScore { get; set; }
        private Score CheckAndSetResult()
        {
            Score result = null;
            if (Games != null)
            {
                int home = 0, away = 0;
                foreach (Game game in Games)
                {
                    if (game.Winner == 1)
                        home++;
                    else if (game.Winner == -1)
                        away++;
                }
                result = new Score(home, away);
            }
            return result;
        }
        public void OnDeserialized(StrengthList listPlayers)
        {
            // Looking for Home and Away Players
            HomePlayer = listPlayers.SearchByAN(Home);
            AwayPlayer = listPlayers.SearchByAN(Away);
            // Looking for empty Games member
            if (Games?.Count == 0)
                Games = null;
            else if (Games != null)
            {
                foreach (Game game in Games)
                    game.OnDeserialized();
                MatchScore?.OnDeserialized();
            }
        }
        public void Presentation()
        {
            Console.WriteLine("{0} {1} - {2} {3} :", HomePlayer.Surname, HomePlayer.Forename, AwayPlayer.Surname, AwayPlayer.Forename);
            if (Games != null)
            {
                foreach (Game game in Games)
                    game.Presentation();
                Console.Write("Score final : ");
                MatchScore.Presentation();
            }
        }
    }
    public class Matches : List<Match>
    {
        public Matches () : base() { }
        public void OnDeserialized(StrengthList listPlayers)
        {
            foreach (Match match in this)
            {
                match.OnDeserialized(listPlayers);
            }
        }
        public void Presentation()
        {
            foreach (Match match in this)
            {
                match.Presentation();
            }
        }
    }
}
