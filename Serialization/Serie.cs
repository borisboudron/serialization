﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Serialization
{
    public enum TeamsOrder
    {
        GRID_ORDER,
        LIST_ORDER
    }
    public class IndexedTeams : Dictionary<int, Team>, IXmlSerializable
    {
        public IndexedTeams()
        {

        }
        XmlSchema IXmlSerializable.GetSchema()
        {
            throw new NotImplementedException();
        }
        void IXmlSerializable.ReadXml(XmlReader reader)
        {
            throw new NotImplementedException();
        }
        void IXmlSerializable.WriteXml(XmlWriter writer)
        {
            throw new NotImplementedException();
        }
        public void Presentation()
        {
            foreach (KeyValuePair<int, Team> pair in this)
            {
                pair.Value.Presentation();
            }
        }
    }
    public class Serie
    {
        public Serie() { }
        public Serie(char serieLevel, int serieDivision, char serieLetter)
        {
            SerieLevel = serieLevel;
            SerieDivision = serieDivision;
            SerieLetter = serieLetter;
            SerieTeams = null;
            SerieMatches = null;
        }
        public Serie(char serieLevel, int serieDivision, char serieLetter, Team[] teams, TeamsOrder order)
        {
            SerieLevel = serieLevel;
            SerieDivision = serieDivision;
            SerieLetter = serieLetter;
            if ((teams.Length == 12) && (order == TeamsOrder.GRID_ORDER))
            {
                List<Team> teamsList = new List<Team>();
                foreach (Team team in teams)
                    teamsList.Add(team);
                setSerieTeams(teamsList, new int[] {8, 1, 10, 3, 12, 5, 2, 7, 4, 9, 6, 11});
                setSerieMatches(SerieTeams);
            }
            else if ((teams.Length == 12) && (order == TeamsOrder.LIST_ORDER))
            {
                List<Team> teamsList = new List<Team>();
                foreach (Team team in teams)
                    teamsList.Add(team);
                setSerieTeams(teamsList, new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12});
                setSerieMatches(SerieTeams);
            }
            else
            {
                SerieTeams = null;
                SerieMatches = null;
                System.Console.Write("Le nombre d'équipes ne correspond pas, il doit valoir 12.");
            }
        }
        public Serie(char serieLevel, int serieDivision, char serieLetter, List<Team> teams, TeamsOrder order)
        {
            SerieLevel = serieLevel;
            SerieDivision = serieDivision;
            SerieLetter = serieLetter;
            if ((teams.Count == 12) && (order == TeamsOrder.GRID_ORDER))
            {
                setSerieTeams(teams, new int[] { 8, 1, 10, 3, 12, 5, 2, 7, 4, 9, 6, 11 });
                setSerieMatches(SerieTeams);
            }
            else if ((teams.Count == 12) && (order == TeamsOrder.LIST_ORDER))
            {
                setSerieTeams(teams, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
                setSerieMatches(SerieTeams);
            }
            else
            {
                SerieTeams = null;
                SerieMatches = null;
                System.Console.Write("Impossible d'instancier la classe Serie avec un nombre d'équipes différent de 12.");
            }
        }
        public char SerieLevel { get; set; }
        public int SerieDivision { get; set; }
        public char SerieLetter { get; set; }
        public IndexedTeams SerieTeams { get; set; }
        public List<InterclubMatchDay> SerieMatches { get; set; }
        public void Presentation()
        {
            Console.WriteLine("Série : {0}{1}{2}", SerieLevel, SerieDivision, SerieLetter);
            SerieTeams?.Presentation();
            foreach(InterclubMatchDay matchDay in SerieMatches)
            {
                matchDay.Presentation();
            }
        }
        private string CompleteSerieLevel(char serieLevel, int serieDivision, char serieLetter)
        {
            return serieLevel + serieDivision.ToString() + serieLetter;
        }
        private char LevelOfSerie(string completeSerieLevel)
        {
            return completeSerieLevel.ToCharArray()[0];
        }
        private int DivisionOfSerie(string completeSerieLevel)
        {
            return Convert.ToInt32(Char.GetNumericValue(completeSerieLevel.ToCharArray()[1]));
        }
        private char LetterOfSerie(string completeSerieLevel)
        {
            return completeSerieLevel.ToCharArray()[2];
        }
        public void setSerieTeams(List<Team> teams, int[] gridPositions)
        {
            SerieTeams = new IndexedTeams();
            for (int i = 0; i < 12; i++)
            {
                SerieTeams.Add(gridPositions[i], teams[i]);
            }
        }
        public void setSerieMatches(Dictionary<int, Team> serieTeams)
        {
            // On se sera au préalable assuré que la taille du dictionnaire est de 12. On revérifie.
            if (serieTeams.Count == 12)
            {
                SerieMatches = new List<InterclubMatchDay>();
                for (int md = 0; md < 22; md++)
                {
                    List<InterclubMatch> matches = new List<InterclubMatch>();
                    for (int m=0;m<6;m++)
                    {
                        //int home = 1;
                        //if (md < 11)
                        //    home = 0;
                        //InterclubTeam homeTeam = new InterclubTeam(serieTeams[Constants.Grid12Teams[m, md-home*11, home]]);
                        //InterclubTeam awayTeam = new InterclubTeam(serieTeams[Constants.Grid12Teams[m, md-homaae*11, 1-home]]);
                        InterclubTeam homeTeam = new InterclubTeam(new Team("Test", 'A', 1));
                        InterclubTeam awayTeam = new InterclubTeam(new Team("Test", 'B', 2));
                        matches.Add(new InterclubMatch (new DateTime(2018, 01, 01), m+1, homeTeam, awayTeam));
                    }
                    SerieMatches.Add(new InterclubMatchDay (md, new DateTime(2018, 01, 01), matches));
                }
            }
            else
            {
                SerieMatches = null;
                System.Console.Write("Impossible de créer un calendrier avec un nombre d'équipes différent de 12.");
            }
        }
    }
}
