﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows;
using System.Xml.Linq;

namespace Serialization
{
    public class PingDataBase
    {
        public PingDataBase() { }
        //public PingDataBase(int season, string clubName, StrengthList playerList, XDocument playerXML)
        //{
        //    Season = season;
        //    ClubName = clubName;
        //    PlayerList = playerList;
        //    PlayerXML = playerXML;
        //}
        public PingDataBase(int season, string longClubName, string clubName, StrengthList playerList, ListOfClubTeams teamList)
        {
            Season = season;
            LongClubName = longClubName;
            ClubName = clubName;
            PlayerList = playerList;
            TeamList = teamList;
        }
        public int Season { get; set; }
        public string LongClubName { get; set; }
        public string ClubName { get; set; }
        public StrengthList PlayerList { get; set; }
        //public XDocument PlayerXML { get; set; }
        public ListOfClubTeams TeamList { get; set; }
        //public XDocument TeamXML { get; set; }
        public void Nothing()
        {

        }
        public void Presentation()
        {
            Console.WriteLine("Saison {0}", Season);
            Console.WriteLine("Club : {0} ({1})", LongClubName, ClubName);
            Console.WriteLine("Liste des joueurs :");
            PlayerList.Presentation();
            TeamList.Presentation();
        }
        public void Save()
        {
            // Save to disk.
            //PlayerXML.Save(@"D:\Projets\PingGestion\PingGestion\players.xml");
        }
        public void Load()
        {
            // Load the XDocument
            //PlayerXML = LinqToXmlObjectModel.GetXmlPlayers();
            // Load the List<Player>
            //PlayerList = LinqToXmlObjectModel.XDocumentToList(PlayerXML);
        }
    }
}
