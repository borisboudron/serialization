﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Serialization
{
    public class InterclubMatchDay
    {
        public InterclubMatchDay() { }
        public InterclubMatchDay(int matchDay, DateTime matchDayDate, List<InterclubMatch> matchDayMatches)
        {
            MatchDay = matchDay;
            MatchDayDate = matchDayDate;
            MatchDayMatches = matchDayMatches;
        }
        public int MatchDay { get; set; }
        public DateTime MatchDayDate { get; set; }
        public List<InterclubMatch> MatchDayMatches { get; set; }
        public void OnDeserialized(StrengthList listPlayers, ListOfTeams listTeams)
        {
            foreach (InterclubMatch iMatch in MatchDayMatches)
            {
                iMatch.OnDeserialized(listPlayers, listTeams);
            }
        }
        public void Presentation()
        {
            Console.WriteLine("Journée {0} ({1})", MatchDay, MatchDayDate);
            foreach (InterclubMatch iMatch in MatchDayMatches)
            {
                iMatch.Presentation();
            }
        }
    }
}
