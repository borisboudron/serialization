﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Serialization
{
    [XmlInclude(typeof(ClubTeam))]
    public class Team
    {
        public Team() { }
        public Team(string club, char letter, int gridPosition)
        {
            Club = club;
            Letter = letter;
            CompleteName = TeamWithLetter(Club, Letter);
            GridPosition = gridPosition;
        }
        public Team(string teamWithLetter, int gridPosition)
        {
            Club = ClubOfTeam(teamWithLetter);
            Letter = LetterOfTeam(teamWithLetter);
            CompleteName = teamWithLetter;
            GridPosition = gridPosition;
        }
        public string Club { get; set; }
        public char Letter { get; set; }
        public string CompleteName { get; set; }
        public int GridPosition { get; set; }
        public void Presentation()
        {
            Console.WriteLine("Equipe : {0} ({1})", CompleteName, GridPosition);
        }
        private string TeamWithLetter(string team, char letter)
        {
            return team + " " + letter;
        }
        private string ClubOfTeam(string teamWithLetter)
        {
            string[] separator = { " " };
            string[] words = teamWithLetter.Split(separator, StringSplitOptions.RemoveEmptyEntries);
            return words[0];
        }
        private char LetterOfTeam(string teamWithLetter)
        {
            string[] separator = { " " };
            string[] words = teamWithLetter.Split(separator, StringSplitOptions.RemoveEmptyEntries);
            return words[1].ToCharArray()[0];
        }
    }
    public class ClubTeam : Team
    {
        public ClubTeam() : base() { }
        public ClubTeam(string club, char letter, int gridPosition)
           : base(club, letter, gridPosition)
        {

        }
        public ClubTeam(string club, char letter, int gridPosition, Serie teamSerie)
          : base(club, letter, gridPosition)
        {
            TeamSerie = teamSerie;
        }
        public Serie TeamSerie { get; set; }
        new public void Presentation()
        {
            base.Presentation();
            TeamSerie.Presentation();
        }
    }
    public class Opponent : Team
    {
        public Opponent(string club, char letter, int gridPosition)
            : base(club, letter, gridPosition)
        {
        }
    }
    public class ListOfTeams : List<Team>
    {
        public ListOfTeams () { }
        public void OnDeserialized()
        {

        }
        public void Presentation()
        {

        }
        public Team SearchInTeamList(string teamName)
        {
            bool found = false;
            int i = 0;
            Team team = null;
            while ((!found) && (i < Count))
            {
                if (this.ElementAt(i).CompleteName == teamName)
                {
                    team = this.ElementAt(i);
                    found = true;
                }
                else
                    i++;
            }
            return team;
        }
    }
    public class ListOfClubTeams : List<ClubTeam>
    {
        public ListOfClubTeams() : base() { }
        public void Presentation()
        {
            foreach (ClubTeam team in this)
            {
                team.Presentation();
            }
        }
    }
}
