﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Serialization
{
    public enum Ranking
    {
        NC = 0,
        E6, E4, E2, E0,
        D6, D4, D2, D0,
        C6, C4, C2, C0,
        B6, B4, B2, B0,
        A
    }
    [XmlInclude(typeof(ClubPlayer))]
    public class Player
    {
        public Player() { }
        public Player(string affiliationNumber, string surname, string forename, Ranking? rank)
        {
            AffiliationNumber = affiliationNumber;
            Surname = surname;
            Forename = forename;
            Rank = rank;
        }
        [XmlAttribute(AttributeName = "AN")]
        public string AffiliationNumber { get; set; }
        public string Surname { get; set; }
        public string Forename { get; set; }
        public Ranking? Rank { get; set; }
        public void Presentation()
        {
            Console.WriteLine("Numéro d'affiliation : {0}", AffiliationNumber);
            Console.WriteLine("{0} {1} ({2})", Forename, Surname, Rank);
        }
    }
    public class ClubPlayer : Player
    {
        public ClubPlayer() : base() { }
        public ClubPlayer(string affiliationNumber, string surname, string forename, Ranking? rank)
            : base(affiliationNumber, surname, forename, rank) { }
        public ClubPlayer(string affiliationNumber, string surname, string forename, DateTime birthDate, Ranking? rank, string address, string postalCode, string city, string phone, string mail)
            : base(affiliationNumber, surname, forename, rank)
        {
            BirthDate = birthDate;
            Address = address;
            PostalCode = postalCode;
            City = city;
            Phone = phone;
            Mail = mail;
        }
        public DateTime? BirthDate { get; set; }
        public string Address { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public string Phone { get; set; }
        public string Mail { get; set; }
        new public void Presentation()
        {
            base.Presentation();
            Console.WriteLine("Date de naissance : {0}", BirthDate);
            Console.WriteLine("Adresse : {0}, {1} {2}", Address, PostalCode, City);
            Console.WriteLine("Numéro de téléphone : {0}", Phone);
            Console.WriteLine("E-mail : {0}", Mail);
        }
    }
    public class StrengthList : List<ClubPlayer>
    {
        public StrengthList() : base() { }
        public Player SearchByAN(string aNumber)
        {
            bool found = false;
            int i = 0;
            Player player = null;
            while((!found)&&(i<Count))
            {
                if (this.ElementAt(i).AffiliationNumber == aNumber)
                {
                    player = this.ElementAt(i);
                    found = true;
                }
                else
                    i++;
            }
            return player;
        }
        public void Presentation()
        {
            foreach (ClubPlayer player in this)
            {
                player.Presentation();
            }
        }
    }
}
